﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MASAS.MSM;
using MASAS.MSM.DomainLayer.Logging;

namespace msmUI
{

    /// <summary>
    /// Service Dialog.
    /// </summary>
    public partial class ServiceConfigDialog : Form
    {

        // Private Members...
        private MasasService masasService = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceConfigDialog"/> class.
        /// </summary>
        public ServiceConfigDialog()
        {
            InitializeComponent();

            btnStop.Enabled = false;

            masasService = MasasService.Instance;

            MasasService.DataStoreEnum dataStore = MasasService.DataStoreEnum.SharePointServer;
            try {
                dataStore = (MasasService.DataStoreEnum)Enum.Parse( typeof( MasasService.DataStoreEnum ), Properties.Settings.Default.MSMDataStore );
            }
            catch(Exception /* ex */ ) {}

            masasService.DataStoreType = dataStore;

            Logger.Instance.LogEvents = Properties.Settings.Default.LogEvents;
            Logger.Instance.LogEventTypes = ( LogEventType )Properties.Settings.Default.LogEventType;
            Logger.Instance.NewLogEntry += new EventHandler<MASAS.MSM.DomainLayer.Logging.LogEventArgs>( Logger_NewLogEntry );
        }

        /// <summary>
        /// Handles the NewLogEntry event of the Logger control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MASAS.MSM.DomainLayer.Logging.LogEventArgs"/> instance containing the event data.</param>
        void Logger_NewLogEntry( object sender, MASAS.MSM.DomainLayer.Logging.LogEventArgs e )
        {
            // Add the new log entry to the list and select it.
            lbLogs.SetSelected( lbLogs.Items.Add( e.LogEntry ), true );
        }

        /// <summary>
        /// Handles the Click event of the btnExit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnExit_Click( object sender, EventArgs e )
        {
            masasService.Stop();

            DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Handles the Click event of the btnStart control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnStart_Click( object sender, EventArgs e )
        {
            masasService.Start();
            btnStop.Enabled = true;
            btnStart.Enabled = false;
        }

        /// <summary>
        /// Handles the Click event of the btnStop control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnStop_Click( object sender, EventArgs e )
        {
            masasService.Stop();
            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }

    }

}