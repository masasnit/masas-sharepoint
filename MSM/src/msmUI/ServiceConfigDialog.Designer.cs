﻿namespace msmUI
{
    partial class ServiceConfigDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.gbLog = new System.Windows.Forms.GroupBox();
            this.lbLogs = new System.Windows.Forms.ListBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.gbLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.btnExit.Location = new System.Drawing.Point( 921, 560 );
            this.btnExit.Margin = new System.Windows.Forms.Padding( 4 );
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size( 100, 28 );
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler( this.btnExit_Click );
            // 
            // gbLog
            // 
            this.gbLog.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
                        | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.gbLog.Controls.Add( this.lbLogs );
            this.gbLog.Location = new System.Drawing.Point( 16, 42 );
            this.gbLog.Margin = new System.Windows.Forms.Padding( 4 );
            this.gbLog.Name = "gbLog";
            this.gbLog.Padding = new System.Windows.Forms.Padding( 4 );
            this.gbLog.Size = new System.Drawing.Size( 1005, 510 );
            this.gbLog.TabIndex = 7;
            this.gbLog.TabStop = false;
            this.gbLog.Text = "Logs";
            // 
            // lbLogs
            // 
            this.lbLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbLogs.FormattingEnabled = true;
            this.lbLogs.ItemHeight = 16;
            this.lbLogs.Location = new System.Drawing.Point( 4, 19 );
            this.lbLogs.Margin = new System.Windows.Forms.Padding( 4 );
            this.lbLogs.Name = "lbLogs";
            this.lbLogs.Size = new System.Drawing.Size( 997, 487 );
            this.lbLogs.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point( 12, 12 );
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size( 75, 23 );
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler( this.btnStart_Click );
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point( 93, 12 );
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size( 75, 23 );
            this.btnStop.TabIndex = 9;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler( this.btnStop_Click );
            // 
            // ServiceConfigDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 8F, 16F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 1037, 603 );
            this.Controls.Add( this.btnStop );
            this.Controls.Add( this.btnStart );
            this.Controls.Add( this.gbLog );
            this.Controls.Add( this.btnExit );
            this.Margin = new System.Windows.Forms.Padding( 4 );
            this.Name = "ServiceConfigDialog";
            this.Text = "MASAS Service Configuration";
            this.gbLog.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox gbLog;
        private System.Windows.Forms.ListBox lbLogs;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
    }
}

