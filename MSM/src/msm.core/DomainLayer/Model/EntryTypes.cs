﻿using System;

namespace MASAS.MSM.DomainLayer.Model
{

    /// <summary>
    /// Category types based on CAP standard.
    /// </summary>
    public enum CategoryTypes
    {
        Other = 0,
        Geo,
        Met,
        Safety,
        Security,
        Rescue,
        Fire,
        Health,
        Env,
        Transport,
        Infra,
        CBRNE
    }

    /// <summary>
    /// Status types based on CAP standard.
    /// </summary>
    public enum StatusTypes
    {
        Test = 0,
        Actual,
        Exercise,
        System,
        Draft
    }

    /// <summary>
    /// Severity types based on CAP standard.
    /// </summary>
    public enum SeverityTypes
    {
        Unknown = 0,
        Extreme,
        Severe,
        Moderate,
        Minor
    }

    /// <summary>
    /// Certainty types based on CAP standard.
    /// </summary>
    public enum CertaintyTypes
    {
        Unknown = 0,
        Observed,
        Likely,
        Possible,
        Unlikely
    }

}