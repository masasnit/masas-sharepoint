﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MASAS.MSM.DomainLayer.Model
{

    /// <summary>
    /// Filter Type enumeration.
    /// </summary>
    public enum FilterItemType
    {

        /// <summary>
        /// Unknown type.
        /// </summary>
        Unknown, 

        /// <summary>
        /// Entry type.
        /// </summary>
        Entry,

        /// <summary>
        /// Alert type.
        /// </summary>
        Alert
    }

    /// <summary>
    /// State of item based on the filters applied.
    /// </summary>
    public enum FilterStateType
    {
        None    = 0,
        Ignore  = 1,
        Monitor = 2,
        Action  = 3
    }

}
