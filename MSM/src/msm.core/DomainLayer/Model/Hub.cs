﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MASAS.MSM.DomainLayer.Model
{

    /// <summary>
    /// Hub Information class
    /// </summary>
    public class Hub
    {

        /// <summary>
        /// Hub access tyep enumeration.
        /// </summary>
        [Flags]
        public enum AccessType
        {
            Read    = 0x01,
            Write   = 0x02,
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the URI.
        /// </summary>
        /// <value>
        /// The URI.
        /// </value>
        public string URI { get; set; }

        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>
        /// The token.
        /// </value>
        public string Token { get; set; }

        /// <summary>
        /// Gets or sets the access type.
        /// </summary>
        /// <value>
        /// The access.
        /// </value>
        public AccessType Access { get; set; }

        /// <summary>
        /// Gets or sets the last polled.
        /// </summary>
        /// <value>
        /// The last polled.
        /// </value>
        public DateTime LastPolled { get; set; }

        /// <summary>
        /// Gets or sets the filters.
        /// </summary>
        /// <value>
        /// The filters.
        /// </value>
        public List<Filter> Filters { get; set; }

    }

}
