﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MASAS.MSM.DomainLayer.Model
{

    /// <summary>
    /// Entry Attachment class.
    /// </summary>
    public class EntryAttachment
    {

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the HRef to the attachment.
        /// </summary>
        /// <value>
        /// The HRef to the attachment.
        /// </value>
        public Uri HRef { get; set; }

        /// <summary>
        /// Gets or sets the content type.
        /// </summary>
        /// <value>
        /// The content type.
        /// </value>
        public string ContentType { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        /// <value>
        /// The length.
        /// </value>
        public long Length { get; set; }

    }

}
