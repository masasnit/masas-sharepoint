﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MASAS.MSM.DomainLayer.Logging
{

    /// <summary>
    /// Log event type enumeration.
    /// </summary>
    public enum LogEventType
    {

        /// <summary>
        /// An information event.  This indicates a significant event that requires no attention.
        /// </summary>
        Information = 0,

        /// <summary>
        /// A warning event.  This indicates a possible problem, buy may not have an immediate impact.
        /// </summary>
        Warning     = 1,

        /// <summary>
        /// An error event.  This indicates a serious issue that needs to be dealt with.
        /// </summary>
        Error       = 2,

    }

}
