﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MASAS.MSM.DomainLayer.Logging
{

    /// <summary>
    /// Log event arguments.
    /// </summary>
    public class LogEventArgs : EventArgs
    {

        // Propeties...

        /// <summary>
        /// Gets the log entry.
        /// </summary>
        public string LogEntry { get; private set; }

        /// <summary>
        /// Gets the type of event.
        /// </summary>
        /// <value>
        /// The type of event.
        /// </value>
        public LogEventType EventType { get; private set; }

        /// <summary>
        /// Gets the event time.
        /// </summary>
        public DateTime EventTime { get; private set; }

        /// <summary>
        /// Gets the exception.
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogEventArgs"/> class.
        /// The event type of the entry will be set to "Information".
        /// </summary>
        /// <param name="logEntry">The log entry.</param>
        public LogEventArgs( string logEntry )
        {
            EventTime   = DateTime.Now;
            EventType   = LogEventType.Information;
            Exception   = null;
            LogEntry    = logEntry;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogEventArgs"/> class.
        /// </summary>
        /// <param name="logEntry">The log entry.</param>
        /// <param name="eventType">Type of the event.</param>
        public LogEventArgs( string logEntry, LogEventType eventType )
        {
            EventTime   = DateTime.Now;
            EventType   = eventType;
            Exception   = null;
            LogEntry    = logEntry;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogEventArgs"/> class.
        /// The event type of the entry will be set to "Error".
        /// </summary>
        /// <param name="logEntry">The log entry.</param>
        /// <param name="exception">The exception.</param>
        public LogEventArgs( string logEntry, Exception exception )
        {
            EventTime   = DateTime.Now;
            EventType   = LogEventType.Error;
            Exception   = exception;
            LogEntry    = logEntry;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format( "{0:G}: {1} - {2}", EventTime, EventType.ToString(), LogEntry );
        }

    }

}
