﻿using System;
using System.Collections.Generic;

using MASAS.MSM.DomainLayer.Model;

namespace MASAS.MSM.DataLayer.DataMapper
{

    public interface IDataStore : IDisposable
    {

        /// <summary>
        /// Gets the name of the storage.
        /// </summary>
        /// <value>
        /// The name of the storage.
        /// </value>
        string StorageName { get; }

        /// <summary>
        /// Gets or sets the current hub.
        /// </summary>
        /// <value>The current hub.</value>
        Hub CurrentHub { get; set; }
        
        /// <summary>
        /// Entries the exists.
        /// </summary>
        /// <param name="ID">The ID.</param>
        /// <returns></returns>
        bool EntryExists( Guid ID );

        /// <summary>
        /// Entry exists by external ID.
        /// </summary>
        /// <param name="ExternalID">The external ID.</param>
        /// <returns>true if entry has been found by it's external ID (string)</returns>
        bool EntryExistsByExternalID( string ExternalID );

        /// <summary>
        /// Adds the entry.
        /// </summary>
        /// <param name="Entry">The entry.</param>
        /// <returns>True if successfull, false otherwise</returns>
        bool AddEntry( HubEntry Entry );

        /// <summary>
        /// Updates the entry.
        /// </summary>
        /// <param name="Entry">The entry.</param>
        /// <returns>True if entry has been updated, false otherwise.</returns>
        bool UpdateEntry( HubEntry Entry );

        /// <summary>
        /// Updates the hub data.
        /// </summary>
        /// <param name="hub">The hub.</param>
        /// <returns>True if the data has been updated, false otherwise.</returns>
        bool UpdateHubData( Hub hub );
        
        /// <summary>
        /// Gets the entry of the given ID.
        /// </summary>
        /// <param name="ID">The ID of the entry.</param>
        /// <returns>The entry if found, null of not found.</returns>
        HubEntry GetEntry( Guid ID );
        
        /// <summary>
        /// Gets the registered hubs.
        /// </summary>
        /// <returns>The registered hubs.</returns>
        List<Hub> GetRegisteredHubs();

    }

}
