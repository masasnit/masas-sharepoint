﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using MASAS.MSM.Services.Data;

namespace MASAS.MSM.Services
{

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPublicationService" in both code and config file together.
    /// <summary>
    /// The Publication service contract definition.
    /// </summary>
    [ServiceContract]
    public interface IPublicationService
    {

        /// <summary>
        /// Publishes a new entry.
        /// </summary>
        /// <param name="hub">The MASAS hub to receive the publication.</param>
        /// <param name="publicationEntry">The entry to be published.</param>
        /// <returns>
        /// The published entry with the additional server data.
        /// </returns>
        [OperationContract]
        FeedEntry PublishEntry( Hub hub, PublicationEntry publicationEntry );

        /// <summary>
        /// Updates an entry.
        /// </summary>
        /// <param name="hub">The MASAS hub to receive the publication.</param>
        /// <param name="entryIdentifier">The entry identifier that needs to be updated.</param>
        /// <param name="publicationEntry">The publication entry with the data to be updated.</param>
        /// <returns>The published entry with the additional server data.</returns>
        [OperationContract]
        FeedEntry UpdateEntry( Hub hub, string entryIdentifier, PublicationEntry publicationEntry );

    }

}
