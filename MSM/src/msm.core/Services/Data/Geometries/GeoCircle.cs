﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MASAS.MSM.Services.Data.Geometries
{

    /// <summary>
    /// GeoRSS Circle data class.
    /// </summary>
    [DataContract]
    class GeoCircle : Geometry
    {

        /// <summary>
        /// Gets or sets the center point.
        /// </summary>
        /// <value>
        /// The center point.
        /// </value>
        [DataMember]
        public GeoPoint CenterPoint { get; set; }

        /// <summary>
        /// Gets or sets the radius.
        /// </summary>
        /// <value>
        /// The radius.
        /// </value>
        [DataMember]
        public double Radius { get; set; }

        /// <summary>
        /// Get the GeoRSS XML for this object.
        /// </summary>
        /// <returns>
        /// The GeoRSS XML.
        /// </returns>
        internal override string ToGeoRSS()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat( "<georss:circle>{0} {1} {2}</georss:circle>", CenterPoint.Latitude, CenterPoint.Longitude, Radius );

            return sb.ToString();
        }

    }

}
