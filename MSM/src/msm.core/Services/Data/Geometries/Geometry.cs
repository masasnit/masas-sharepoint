﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MASAS.MSM.Services.Data.Geometries
{

    /// <summary>
    /// Abstract base class for the GeoRSS geometries.
    /// </summary>
    [DataContract]
    public abstract class Geometry
    {

        /// <summary>
        /// Get the GeoRSS XML for this object.
        /// </summary>
        /// <returns>The GeoRSS XML.</returns>
        internal abstract string ToGeoRSS();

    }

}
