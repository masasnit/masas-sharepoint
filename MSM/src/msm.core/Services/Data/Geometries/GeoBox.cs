﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MASAS.MSM.Services.Data.Geometries
{

    /// <summary>
    /// GeoRSS Box data class.
    /// </summary>
    [DataContract]
    class GeoBox : Geometry
    {

        /// <summary>
        /// Gets or sets the lower corner.
        /// </summary>
        /// <value>
        /// The lower corner.
        /// </value>
        [DataMember]
        public GeoPoint LowerCorner { get; set; }

        /// <summary>
        /// Gets or sets the upper corner.
        /// </summary>
        /// <value>
        /// The upper corner.
        /// </value>
        [DataMember]
        public GeoPoint UpperCorner { get; set; }

        /// <summary>
        /// Get the GeoRSS XML for this object.
        /// </summary>
        /// <returns>
        /// The GeoRSS XML.
        /// </returns>
        internal override string ToGeoRSS()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat( "<georss:box>{0} {1} {2} {3}</georss:box>", LowerCorner.Latitude, LowerCorner.Longitude, UpperCorner.Latitude, UpperCorner.Longitude );

            return sb.ToString();
        }

    }

}
