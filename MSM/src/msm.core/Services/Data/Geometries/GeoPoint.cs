﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MASAS.MSM.Services.Data.Geometries
{

    /// <summary>
    /// GeoRSS Point data class.
    /// </summary>
    [DataContract]
    class GeoPoint : Geometry
    {

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>
        /// The latitude.
        /// </value>
        [DataMember]
        public double Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>
        /// The longitude.
        /// </value>
        [DataMember]
        public double Longitude { get; set; }

        /// <summary>
        /// Get the GeoRSS XML for this object.
        /// </summary>
        /// <returns>
        /// The GeoRSS XML.
        /// </returns>
        internal override string ToGeoRSS()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat( "<georss:point>{0} {1}</georss:point>", Latitude, Longitude );

            return sb.ToString();
        }

    }

}
