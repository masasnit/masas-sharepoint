﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MASAS.MSM.Services.Data.Geometries
{

    /// <summary>
    /// GeoRSS Line data class.
    /// </summary>
    [DataContract]
    class GeoLine : Geometry
    {

        /// <summary>
        /// Gets or sets the points.
        /// </summary>
        /// <value>
        /// The points.
        /// </value>
        [DataMember]
        public List<GeoPoint> Points { get; set; }

        /// <summary>
        /// Get the GeoRSS XML for this object.
        /// </summary>
        /// <returns>
        /// The GeoRSS XML.
        /// </returns>
        internal override string ToGeoRSS()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append( "<georss:line>" );

            bool first = true;
            foreach( GeoPoint point in Points )
            {
                if( !first )
                {
                    sb.Append( " " );
                }
                else
                {
                    first = false;
                }

                sb.AppendFormat( "{0} {1}", point.Latitude, point.Longitude );
            }
            sb.Append( "</georss:line>" );

            return sb.ToString();
        }

    }

}
