﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using MASAS.MSM.DomainLayer.Model;
using MASAS.MSM.Services.Data.Geometries;

namespace MASAS.MSM.Services.Data
{

    /// <summary>
    /// A simple class to contain the required publication data.
    /// </summary>
    [DataContract]
    [KnownType( typeof( GeoBox ) )]
    [KnownType( typeof( GeoCircle ) )]
    [KnownType( typeof( GeoLine ) )]
    [KnownType( typeof( GeoPoint ) )]
    [KnownType( typeof( GeoPolygon ) )]
    public class PublicationEntry
    {

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [DataMember]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        [DataMember]
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets the summary.
        /// </summary>
        /// <value>
        /// The summary.
        /// </value>
        [DataMember]
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the expiration.
        /// </summary>
        /// <value>
        /// The expiration.
        /// </value>
        [DataMember]
        public DateTime Expiration { get; set; }

        /// <summary>
        /// Gets or sets the related links.
        /// </summary>
        /// <value>
        /// The related links.
        /// </value>
        [DataMember]
        public List<string> RelatedLinks { get; set; }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        /// <value>
        /// The categories.
        /// </value>
        [DataMember]
        public List<CategoryTypes> Categories { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [DataMember]
        public StatusTypes Status { get; set; }

        /// <summary>
        /// Gets or sets the severities.
        /// </summary>
        /// <value>
        /// The severities.
        /// </value>
        [DataMember]
        public List<SeverityTypes> Severities { get; set; }

        /// <summary>
        /// Gets or sets the certainties.
        /// </summary>
        /// <value>
        /// The certainties.
        /// </value>
        [DataMember]
        public List<CertaintyTypes> Certainties { get; set; }

        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        [DataMember]
        public string Icon { get; set; }

        /// <summary>
        /// Gets or sets the geometries.
        /// </summary>
        /// <value>
        /// The geometries.
        /// </value>
        [DataMember]
        public List<Geometry> Geometries { get; set; }

        /// <summary>
        /// Gets or sets the attachments.
        /// </summary>
        /// <value>
        /// The attachments.
        /// </value>
        [DataMember]
        public Attachment[] Attachments { get; set; }

    }

}
