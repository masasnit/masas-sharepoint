﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using MASAS.MSM.DomainLayer.Model;

namespace MASAS.MSM.Services.Data
{

    /// <summary>
    /// A simple class to contain the complete feed data.
    /// </summary>
    [DataContract]
    public class FeedEntry : PublicationEntry
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        /// <value>
        /// The author.
        /// </value>
        [DataMember]
        public EntryAuthor Author { get; set; }

        /// <summary>
        /// Gets or sets the published date/time.
        /// </summary>
        /// <value>
        /// The published date/time.
        /// </value>
        [DataMember]
        public DateTime Published { get; set; }

        /// <summary>
        /// Gets or sets the last updated date/time.
        /// </summary>
        /// <value>
        /// The last updated date/time.
        /// </value>
        [DataMember]
        public DateTime LastUpdated { get; set; }

    }

}
