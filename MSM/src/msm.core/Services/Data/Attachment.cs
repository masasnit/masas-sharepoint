﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using MASAS.MSM.DomainLayer.Model;
using MASAS.MSM.Services.Data.Geometries;

namespace MASAS.MSM.Services.Data
{

    /// <summary>
    /// Attachment data class.
    /// </summary>
    [DataContract]
    public class Attachment
    {

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [DataMember]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        [DataMember]
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the content type.
        /// </summary>
        /// <value>
        /// The content type.
        /// </value>
        [DataMember]
        public string ContentType { get; set; }

        /// <summary>
        /// Gets or sets the Base64.
        /// </summary>
        /// <value>
        /// The Base64.
        /// </value>
        [DataMember]
        public string Base64 { get; set; }

    }

}
