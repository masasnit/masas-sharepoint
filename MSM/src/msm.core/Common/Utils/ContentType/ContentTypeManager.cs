﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;

using MASAS.MSM.DomainLayer.Logging;

namespace MASAS.MSM.Common.Utils.ContentType
{

    /// <summary>
    /// Content Type Manager.
    /// </summary>
    public class ContentTypeManager
    {

        // Private members...
        private List< ContentTypeLibrary > _contentTypeLibs;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymbolManager"/> class.
        /// </summary>
        public ContentTypeManager()
        {
            _contentTypeLibs = new List<ContentTypeLibrary>();

            string path = System.IO.Path.GetDirectoryName( System.Reflection.Assembly.GetEntryAssembly().Location ) + @"\libraries\";

            // Load the Content Types
            // NOTE: This is hardcoded for now, future may 
            //       see a cleaner way of doing this.

            // Load the Content Type values...
            LoadContentTypeLibrary( path + "ContentTypeLibrary.xml" );
        }

        /// <summary>
        /// Gets the Content Type.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public string GetContentType( string extension )
        {
            string retValue = string.Empty;

            foreach( ContentTypeLibrary ctLib in _contentTypeLibs )
            {
                retValue = ctLib.GetContentType( extension );

                if( retValue != string.Empty )
                {
                    break;
                }
            }

            return retValue;
        }

        public bool IsValid( string contentType )
        {
            bool retValue = false;

            foreach( ContentTypeLibrary ctLib in _contentTypeLibs )
            {
                retValue = ctLib.IsValid( contentType );

                if( retValue == true )
                {
                    break;
                }
            }

            return retValue;
        }

        /// <summary>
        /// Loads the symbol library at the given path.
        /// </summary>
        /// <param name="libraryPath">The library path.</param>
        private void LoadContentTypeLibrary( string libraryPath )
        {
            ContentTypeLibrary  contentTypes    = null;
            XmlSerializer       serializer      = new XmlSerializer( typeof( ContentTypeLibrary ) );

            try
            {
                using( XmlReader reader = XmlReader.Create( libraryPath ) )
                {
                    contentTypes = (ContentTypeLibrary)serializer.Deserialize( reader );
                }
            }
            catch( Exception ex )
            {
                Logger.AddLogEntry( "An unexpected exception has occured!", ex );
            }

            if( contentTypes != null )
            {
                _contentTypeLibs.Add( contentTypes );
            }
        }

    }

}
