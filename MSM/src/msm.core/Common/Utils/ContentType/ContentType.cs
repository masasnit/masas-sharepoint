﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MASAS.MSM.Common.Utils.ContentType
{

    /// <summary>
    /// Content Type class.
    /// </summary>
    [Serializable()]
    public class ContentType
    {

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [XmlText]
        public string value { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [XmlAttribute( "ext" )]
        public string ext { get; set; }

    }

}
