﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MASAS.MSM.Common.Utils.ContentType
{

    /// <summary>
    /// Content Type Library.
    /// </summary>
    [Serializable()]
    [XmlRootAttribute( "ContentTypeLibrary", IsNullable = false )]
    public class ContentTypeLibrary
    {

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlElement( "Name" )]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the symbols.
        /// </summary>
        /// <value>
        /// The symbols.
        /// </value>
        [XmlArray]
        public List<ContentType> ContentTypes { get; set; }

        /// <summary>
        /// Gets the Content Type.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public string GetContentType( string extension )
        {
            string retValue = string.Empty;

            ContentType foundCT  = null;

            try
            {
                foundCT = ContentTypes.First( x => x.ext.ToUpper() == extension.ToUpper() );
            }
            catch( Exception /* ex */ )
            {
                // not found...
                foundCT = null;
            }

            if( foundCT != null )
            {
                retValue = foundCT.value;
            }

            return retValue;
        }

        public bool IsValid( string contentType )
        {
            bool retValue = false;

            ContentType foundCT  = null;

            try
            {
                foundCT = ContentTypes.First( x => x.value.ToUpper() == contentType.ToUpper() );
            }
            catch( Exception /* ex */ )
            {
                // not found...
                foundCT = null;
            }

            if( foundCT != null )
            {
                retValue = true;
            }

            return retValue;
        }

    }

}
