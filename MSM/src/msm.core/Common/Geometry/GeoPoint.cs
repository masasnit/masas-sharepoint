﻿using System;

namespace MASAS.Common.Geometry
{

    /// <summary>
    /// A geographic point.
    /// </summary>
    public class GeoPoint
    {

        // Internal members...
        private double latitude     = 0.0;
        private double longitude    = 0.0;

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>
        /// The latitude.
        /// </value>
        public double Latitude {
            get { return latitude; }
            set { latitude = value; }
        }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>
        /// The longitude.
        /// </value>
        public double Longitude {
            get { return longitude; }
            set { longitude = value; }
        }
        
        /// <summary>
        /// Gets or sets the X-coordinate (longitude).
        /// </summary>
        /// <value>
        /// The X-coordinate (longitude).
        /// </value>
        public double X {
            get { return Longitude; }
            set { Longitude = value; }
        }

        /// <summary>
        /// Gets or sets the Y-coordinate (latitude).
        /// </summary>
        /// <value>
        /// The Y-coordinate (latitude).
        /// </value>
        public double Y {
            get { return Latitude; }
            set { Latitude = value; }
        }

    }

}
