﻿using System;

namespace MASAS.Common.Geometry
{

    public class GeoEnvelope
    {

        // Internal members...
        private GeoPoint topLeft;
        private GeoPoint bottomRight;

        /// <summary>
        /// Gets or sets the top left of the envelope.
        /// </summary>
        /// <value>
        /// The top left of the envelope.
        /// </value>
        public GeoPoint TopLeft {
            get { return topLeft; }
            set { topLeft = value; }
        }

        /// <summary>
        /// Gets or sets the bottom right of the envelope.
        /// </summary>
        /// <value>
        /// The bottom right of the envelope.
        /// </value>
        public GeoPoint BottomRight {
            get { return bottomRight; }
            set { bottomRight = value; }
        }

    }

}
