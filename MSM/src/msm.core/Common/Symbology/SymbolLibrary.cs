﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MASAS.MSM.Common.Symbology
{

    /// <summary>
    /// Symbol Library.
    /// </summary>
    [Serializable()]
    [XmlRootAttribute( "SymbolLibrary", IsNullable = false )]
    public class SymbolLibrary
    {

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlElement( "Name" )]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the symbols.
        /// </summary>
        /// <value>
        /// The symbols.
        /// </value>
        [XmlArray]
        public List<Symbol> Symbols { get; set; }

        /// <summary>
        /// Validates the symbol key.
        /// </summary>
        /// <param name="symbolKey">The symbol key.</param>
        /// <returns></returns>
        public string ValidateSymbolKey( string symbolKey )
        {
            string retValue = string.Empty;
            string keyToFind = symbolKey.Replace('/','.');

            Symbol foundSymbol  = null;

            try
            {
                foundSymbol = Symbols.First( x => x.Key.ToUpper() == keyToFind.ToUpper() );
            }
            catch( Exception /* ex */ )
            {
                // not found...
                foundSymbol  = null;
            }

            if( foundSymbol != null )
            {
                retValue = foundSymbol.Key.Replace('.','/');
            }

            return retValue;
        }

    }

}
