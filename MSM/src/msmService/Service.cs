﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using MASAS.MSM;
using MASAS.MSM.DomainLayer.Logging;

namespace msmService
{

    /// <summary>
    /// The entry point for the Windows Service.
    /// </summary>
    public partial class Service : ServiceBase
    {

        // Private members...
        private bool            _isRunningInConsole = false;
        private MasasService    _masasService       = MasasService.Instance;
        private Logger          _logger             = Logger.Instance;

        /// <summary>
        /// Initializes a new instance of the <see cref="Service"/> class.
        /// </summary>
        public Service()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Setup the object to run in a console.
        /// </summary>
        public void RunInConsole()
        {
            _isRunningInConsole = true;
            OnStart( null );
            Console.WriteLine( "Press any key to stop program" );
            Console.Read();
            OnStop();
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart( string[] args )
        {
            MasasService.DataStoreEnum dataStore = MasasService.DataStoreEnum.SharePointServer;
            try {
                dataStore = (MasasService.DataStoreEnum)Enum.Parse( typeof( MasasService.DataStoreEnum ), Properties.Settings.Default.MSMDataStore );
            }
            catch( Exception /* ex */ ) { }

            _masasService.DataStoreType = dataStore;

            _logger.LogEvents = Properties.Settings.Default.LogEvents;
            _logger.LogEventTypes = (LogEventType)Properties.Settings.Default.LogEventType;

            // Add the log handler...
            _logger.NewLogEntry += new EventHandler<LogEventArgs>( MasasLogger_NewLogEntry );

            Logger.AddLogEntry( "Starting MASAS Service.", LogEventType.Information );
            _masasService.Start();
            Logger.AddLogEntry( "MASAS Service started.", LogEventType.Information );
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            Logger.AddLogEntry( "Stopping MASAS Service.", LogEventType.Information );
            _masasService.Stop();
            Logger.AddLogEntry( "MASAS Service stopped.", LogEventType.Information );

            _logger.NewLogEntry -= new EventHandler<LogEventArgs>( MasasLogger_NewLogEntry );
        }

        /// <summary>
        /// Handles the NewLogEntry event of the MasasLogger control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MASAS.MSM.DomainLayer.Logging.LogEventArgs"/> instance containing the event data.</param>
        private void MasasLogger_NewLogEntry( object sender, LogEventArgs e )
        {
            if( _isRunningInConsole )
            {
                // Dump the log to console...
                Console.WriteLine( e.ToString() );
            }
        }

    }

}
