﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace msmService
{

    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main( string[] args )
        {
            // Run as a Windows service...
            Service service = new Service();

            if( args.Length > 0 && args[0].ToUpper() == "CONSOLE" )
            {
                // Run as a console...
                service.RunInConsole();
            }
            else
            {
                // Run as a service...
                ServiceBase.Run( service );
            }
        }

    }

}
