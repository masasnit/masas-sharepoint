﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

using MASAS.MSM.DomainLayer.Logging;
using MASAS.MSM.SharePoint.REST.OAuth;

namespace MASAS.MSM.SharePoint.REST.Config
{

    /// <summary>
    /// Class SiteConfigurationManager.
    /// </summary>
    internal class SiteConfigurationManager
    {

        // Private members...

        private SiteConfigDataCollection    _siteConfigurations;
        private string                      _filePath = @".\MASAS_SharePointSites.config";

        // Public accessors...

        /// <summary>
        /// Gets the site configurations.
        /// </summary>
        /// <value>The site configurations.</value>
        internal List<SiteConfigData> SiteConfigurations { get { return new List<SiteConfigData>( _siteConfigurations.Sites ); } }

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteConfigurationManager"/> class.
        /// </summary>
        internal SiteConfigurationManager()
        {
            _siteConfigurations = new SiteConfigDataCollection();

            LoadDataFromFile();

            if( _siteConfigurations.OAuth != null )
            {
                TokenHelper.ConfigureToken( _siteConfigurations.OAuth.ClientId, _siteConfigurations.OAuth.ClientSecret, 
                                            _siteConfigurations.OAuth.ClientSigningCertificatePath,
                                            _siteConfigurations.OAuth.ClientSigningCertificatePassword );
            }
        }

        /// <summary>
        /// Loads the data from file.
        /// </summary>
        private void LoadDataFromFile()
        {
            if( File.Exists( _filePath ) )
            {
                SiteConfigDataCollection    siteConfigs = null;
                XmlSerializer               serializer  = new XmlSerializer( typeof( SiteConfigDataCollection ) );

                try
                {
                    using( XmlReader reader = XmlReader.Create( _filePath ) )
                    {
                        siteConfigs = (SiteConfigDataCollection)serializer.Deserialize( reader );
                    }
                }
                catch( Exception ex )
                {
                    Logger.AddLogEntry( "An error occurred while trying to read the MASAS SharePoint configuration file!", ex );
                }

                if( siteConfigs != null )
                {
                    _siteConfigurations = siteConfigs;
                }
            }
            else
            {
                CreateTemplateFile();
            }
        }

        /// <summary>
        /// Creates the template file.
        /// </summary>
        private void CreateTemplateFile()
        {
            SiteConfigData configData = new SiteConfigData();
            _siteConfigurations.Sites.Add( configData );

            try
            {
                XmlSerializer serializer = new XmlSerializer( typeof( SiteConfigDataCollection ) );
                StreamWriter writer = new StreamWriter( _filePath );
                serializer.Serialize( writer, _siteConfigurations );
                writer.Close();
            }
            catch( Exception ex )
            {
                Logger.AddLogEntry( "An error occurred while trying to create a MASAS SharePoint configuration file!", ex );
            }

            _siteConfigurations.Sites.Clear();
        }

    }

}
