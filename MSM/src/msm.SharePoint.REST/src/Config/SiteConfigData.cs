﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MASAS.MSM.SharePoint.REST.Config
{

    /// <summary>
    /// Class SiteConfigData.
    /// </summary>
    [Serializable()]
    public class SiteConfigData
    {

        /// <summary>
        /// Enum accessTypeEnum
        /// </summary>
        public enum accessTypeEnum
        {
            /// <summary>
            /// The default access
            /// </summary>
            defaultAccess,
            /// <summary>
            /// The credential access
            /// </summary>
            credentialAccess,
            /// <summary>
            /// The token access
            /// </summary>
            tokenAccess 
        }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the user password.
        /// </summary>
        /// <value>The user password.</value>
        public string UserPassword { get; set; }

        /// <summary>
        /// Gets or sets the user domain.
        /// </summary>
        /// <value>The user domain.</value>
        public string UserDomain { get; set; }

        /// <summary>
        /// Gets or sets the type of the access.
        /// </summary>
        /// <value>The type of the access.</value>
        public accessTypeEnum AccessType { get; set; }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>The access token.</value>
        [XmlIgnore]
        public string AccessToken { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteConfigData"/> class.
        /// </summary>
        public SiteConfigData()
        {
            AccessType = SiteConfigData.accessTypeEnum.defaultAccess;
            Url = "";
            UserName = "";
            UserDomain = "";
            UserPassword = "";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteConfigData"/> class.
        /// </summary>
        /// <param name="url">The URL.</param>
        public SiteConfigData( string url )
        {
            this.Url = url;

            AccessType = SiteConfigData.accessTypeEnum.defaultAccess;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteConfigData"/> class.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="accessType">Type of the access.</param>
        public SiteConfigData( string url, accessTypeEnum accessType )
        {
            this.Url = url;

            AccessType = accessType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteConfigData"/> class.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="userPassword">The user password.</param>
        /// <param name="userDomain">The user domain.</param>
        public SiteConfigData( string url, string userName, string userPassword, string userDomain = "" )
        {
            this.Url = url;
            this.UserName = userName;
            this.UserPassword = userPassword;
            this.UserDomain = userDomain;

            AccessType = SiteConfigData.accessTypeEnum.credentialAccess;
        }
        
    }

}
