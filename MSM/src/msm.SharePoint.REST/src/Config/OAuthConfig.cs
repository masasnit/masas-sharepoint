﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MASAS.MSM.SharePoint.REST.Config
{

    /// <summary>
    /// Class OAuthConfig.
    /// </summary>
    [Serializable()]
    public class OAuthConfig
    {

        /// <summary>
        /// Gets or sets the client identifier.
        /// </summary>
        /// <value>The client identifier.</value>
        public string ClientId { get; set; }

        /// <summary>
        /// Gets or sets the client secret.
        /// </summary>
        /// <value>The client secret.</value>
        public string ClientSecret { get; set; }

        /// <summary>
        /// Gets or sets the client signing certificate path.
        /// </summary>
        /// <value>The client signing certificate path.</value>
        public string ClientSigningCertificatePath { get; set; }

        /// <summary>
        /// Gets or sets the client signing certificate password.
        /// </summary>
        /// <value>The client signing certificate password.</value>
        public string ClientSigningCertificatePassword { get; set; }

    }

}
