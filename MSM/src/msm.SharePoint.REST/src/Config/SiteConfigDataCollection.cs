﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MASAS.MSM.SharePoint.REST.Config
{

    /// <summary>
    /// Class SiteConfigDataCollection.
    /// </summary>
    [Serializable()]
    [XmlRootAttribute( "SiteConfigurations", IsNullable = false )]
    public class SiteConfigDataCollection
    {

        /// <summary>
        /// The o authentication
        /// </summary>
        public OAuthConfig OAuth;

        /// <summary>
        /// The sites
        /// </summary>
        [XmlArray]
        public List<SiteConfigData> Sites;

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteConfigDataCollection"/> class.
        /// </summary>
        public SiteConfigDataCollection()
        {
            Sites = new List<SiteConfigData>();
        }
        
    }

}
