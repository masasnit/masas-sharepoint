﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MASAS.MSM.DomainLayer.Model;

namespace MASAS.MSM.SharePoint.REST
{

    /// <summary>
    /// Site class to contain site specific information.
    /// </summary>
    internal class SharePointHub : Hub
    {

        /// <summary>
        /// Gets or sets the site URL.
        /// </summary>
        /// <value>
        /// The site URL.
        /// </value>
        internal string SiteUrl { get; set; }

        /// <summary>
        /// Gets or sets the item identifier.
        /// </summary>
        /// <value>
        /// The item identifier.
        /// </value>
        internal int ItemIdentifier { get; set; }

    }

}
