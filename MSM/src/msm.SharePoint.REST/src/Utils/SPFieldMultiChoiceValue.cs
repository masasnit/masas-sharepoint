﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MASAS.MSM.SharePoint.REST.Utils
{

    /// <summary>
    /// Class SPFieldMultiChoiceValue.
    /// </summary>
    internal class SPFieldMultiChoiceValue
    {

        // Private members...
        private List<string> _values = new List<string>();

        // Public accessors...

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>The count.</value>
        internal int Count { get { return _values.Count; } }

        /// <summary>
        /// Gets the <see cref="System.String"/> at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>System.String.</returns>
        internal string this[int index] { get { return _values[index]; } }

        /// <summary>
        /// Creates a SPFieldMultiChoiceValue using the specified attributes.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <returns>SPFieldMultiChoiceValue.</returns>
        internal static SPFieldMultiChoiceValue Create( Dictionary<string, object> attributes )
        {
            SPFieldMultiChoiceValue choiceValue = null;

            if( attributes != null )
            {
                object tempObject;

                if( attributes.TryGetValue( "results", out tempObject ) )
                {
                    ArrayList values = tempObject as ArrayList;
                    if( values != null )
                    {
                        choiceValue = new SPFieldMultiChoiceValue();

                        foreach( string value in values )
                        {
                            choiceValue._values.Add( value );
                        }
                    }
                }
            }

            return choiceValue;
        }

    }

}
