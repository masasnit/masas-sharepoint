﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MASAS.MSM.SharePoint.REST.Utils
{

    /// <summary>
    /// Class SPFieldUrlValue.
    /// </summary>
    internal class SPFieldUrlValue
    {

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        internal string Description { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        internal string Url { get; set; }

        /// <summary>
        /// Creates a SPFieldUrlValue using the specified attributes.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <returns>SPFieldUrlValue.</returns>
        internal static SPFieldUrlValue Create( Dictionary<string, object> attributes )
        {
            SPFieldUrlValue fieldUrl = null;

            if( attributes != null )
            {
                object tempObject;
                
                fieldUrl = new SPFieldUrlValue();

                if( attributes.TryGetValue( "Description", out tempObject ) )
                {
                    fieldUrl.Description = tempObject as string;
                }
                if( attributes.TryGetValue( "Url", out tempObject ) )
                {
                    fieldUrl.Url = tempObject as string;
                }
            }

            return fieldUrl;
        }

    }

}
