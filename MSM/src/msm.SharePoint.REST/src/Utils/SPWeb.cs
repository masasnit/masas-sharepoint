﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

using MASAS.MSM.SharePoint.REST.Config;

namespace MASAS.MSM.SharePoint.REST.Utils
{

    /// <summary>
    /// Class SPWeb.
    /// </summary>
    internal class SPWeb
    {

        // Private members...
        
        private Dictionary<string, SPList> _lists = new Dictionary<string, SPList>();

        // Public accessors....

        /// <summary>
        /// Gets the client context.
        /// </summary>
        /// <value>The client context.</value>
        internal ClientContext ClientContext { get; private set; }

        /// <summary>
        /// Gets the configuration data.
        /// </summary>
        /// <value>The configuration data.</value>
        internal SiteConfigData ConfigData { get; private set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        internal string Title { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        internal string Url { get; set; }

        /// <summary>
        /// Gets the webs.
        /// </summary>
        /// <value>The webs.</value>
        internal List<SPWeb> Webs { get { return GetWebs(); } }

        /// <summary>
        /// Initializes a new instance of the <see cref="SPWeb"/> class.
        /// </summary>
        /// <param name="scData">The sc data.</param>
        internal SPWeb( SiteConfigData scData )
        {
            ConfigData = scData;

            ClientContext = new ClientContext( scData );
            GetAttributes();
        }

        /// <summary>
        /// Gets the webs.
        /// </summary>
        /// <returns>List&lt;SPWeb&gt;.</returns>
        private List<SPWeb> GetWebs()
        {
            List<SPWeb> webs = new List<SPWeb>();

            string response = SPHelper_REST.RequestData( Url + "/_api/web/webs?$select=Title,Url", ConfigData );

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            Dictionary<string, object> root = jsSerializer.Deserialize<Dictionary<string, object>>( response );

            object tempObject;
            if( root != null && root.TryGetValue( "d", out tempObject ) && tempObject is Dictionary<string, object> )
            {
                Dictionary<string,object> data = tempObject as Dictionary<string, object>;

                if( data.TryGetValue( "results", out tempObject ) && tempObject is ArrayList )
                {
                    ArrayList results = tempObject as ArrayList;

                    foreach( object item in results )
                    {
                        if( item is Dictionary<string, object> )
                        {
                            
                            Dictionary<string, object> webObject = item as Dictionary<string, object>;

                            SPWeb web = new SPWeb( ConfigData );
                            GetAttributes( webObject, web );
                            webs.Add( web );
                        }

                    }
                }
            }

            return webs;
        }

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        private void GetAttributes()
        {
            string response = SPHelper_REST.RequestData( ConfigData.Url + "/_api/web?$select=Title,Url", ConfigData );

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            Dictionary<string, object> root = jsSerializer.Deserialize<Dictionary<string, object>>( response );
            
            object tempObject;
            if( root != null && root.TryGetValue( "d", out tempObject ) && tempObject is Dictionary<string, object> )
            {
                Dictionary<string,object> webObject = tempObject as Dictionary<string, object>;

                GetAttributes( webObject, this );
            }
        }

        /// <summary>
        /// Gets the attributes from the dictionary and assigns them to the web object.
        /// </summary>
        /// <param name="webObject">The web object.</param>
        /// <param name="web">The web.</param>
        private void GetAttributes( Dictionary<string,object> webObject, SPWeb web )
        {
            object tempObject;
            if( webObject.TryGetValue( "Title", out tempObject ) )
            {
                web.Title = tempObject as string;
            }
            if( webObject.TryGetValue( "Url", out tempObject ) )
            {
                web.Url = tempObject as string;
            }
        }

        /// <summary>
        /// Gets a list by title.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <returns>SPList.</returns>
        internal SPList GetListByTitle( string title )
        {
            SPList newList = null;

            if( _lists.TryGetValue( title, out newList ) == false )
            {
                string response = SPHelper_REST.RequestData( ConfigData.Url + "/_api/web/lists/getbytitle('" + title + "')", ConfigData );

                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                Dictionary<string, object> root = jsSerializer.Deserialize<Dictionary<string, object>>( response );

                object tempObject;
                if( root != null && root.TryGetValue( "d", out tempObject ) && tempObject is Dictionary<string, object> )
                {
                    Dictionary<string,object> listObject = tempObject as Dictionary<string, object>;

                    newList = SPList.Create( this, listObject );
                    _lists[title] = newList;
                }
            }

            return newList;
        }

    }

}
