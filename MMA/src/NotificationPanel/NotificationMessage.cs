﻿using System;

using Microsoft.SharePoint.Client;

namespace NotificationPanel
{

    /// <summary>
    /// Notification Message class.
    /// </summary>
    public class NotificationMessage
    {

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string   Title           { get; set; }

        /// <summary>
        /// Gets or sets the item identifier.
        /// </summary>
        /// <value>
        /// The item identifier.
        /// </value>
        public int      ItemIdentifier  { get; set; }

        /// <summary>
        /// Gets or sets the item title.
        /// </summary>
        /// <value>
        /// The item title.
        /// </value>
        public string   ItemTitle       { get; set; }

        /// <summary>
        /// Gets or sets the importance.
        /// </summary>
        /// <value>
        /// The importance.
        /// </value>
        public string   Importance      { get; set; }

        /// <summary>
        /// Gets or sets the updated.
        /// </summary>
        /// <value>
        /// The updated.
        /// </value>
        public DateTime Updated         { get; set; }

        /// <summary>
        /// Gets the importance ID.
        /// </summary>
        public int ImportanceID
        {
            get
            {
                int retValue = 0;

                if( Importance == "Ignore" )
                {
                    retValue = 0;
                }
                else if( Importance == "Monitor" )
                {
                    retValue = 1;
                }
                else if( Importance == "Action" )
                {
                    retValue = 2;
                }

                return retValue;
            }
        }

        /// <summary>
        /// Gets the item URI.
        /// </summary>
        public Uri ItemURI
        {
            get
            {
                string uri = ApplicationContext.Current.Url + "/Lists/MASAS%20Hub%20Items/DispForm.aspx?ID=" + ItemIdentifier.ToString();
                return new Uri( uri );
            }
        }

    }

}
