﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using ESRI.ArcGIS.Client;
using ESRI.ArcGIS.Client.Extensibility;

namespace SymbolAssociationCommand
{

    /// <summary>
    /// Select Layer Dialog.
    /// Allows a user to select a Graphics Layer.
    /// </summary>
    public partial class SelectLayerDialog : ChildWindow
    {

        // Private Members...
        private Map map = null;

        // Public Members...
        public Layer SelectedLayer { get; set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectLayerDialog"/> class.
        /// </summary>
        /// <param name="map">The ArcGIS map containing the Layers we are interested in.</param>
        public SelectLayerDialog( ESRI.ArcGIS.Client.Map map )
        {
            // Assign the internal members...
            this.map = map;
            
            // Initialize the UI components...
            InitializeComponent();

            // Disable the OK button for now...
            OKButton.IsEnabled = false;

            // Populate the list with the necessary layers...
            foreach( Layer layer in map.Layers )
            {
                if( layer is GraphicsLayer )
                {
                    ListBoxItem item = new ListBoxItem();
                    item.Content = layer.GetValue( MapApplication.LayerNameProperty ) as string;
                    item.Tag = layer;

                    lbLayers.Items.Add( item );
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the OKButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void OKButton_Click( object sender, RoutedEventArgs e )
        {   
            // Let's save the layer...
            ListBoxItem item = ( ListBoxItem )lbLayers.SelectedItem;
            SelectedLayer = (Layer)item.Tag;

            // OK was clicked...
            this.DialogResult = true;
        }

        /// <summary>
        /// Handles the Click event of the CancelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void CancelButton_Click( object sender, RoutedEventArgs e )
        {
            // Cancel was clicked...
            this.DialogResult = false;
        }

        /// <summary>
        /// Handles the SelectionChanged event of the lbLayers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void lbLayers_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            // The List has a valid selection, we can let the user move in if needed...
            OKButton.IsEnabled = true;
        }

    }

}

