using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Security;
using System.Globalization;
using System.Diagnostics;

namespace PublishingWorkflowAction.Features.MASASPublishingWFFeature
{

    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>
    [Guid( "eb554dac-057f-4403-826c-ba84f650545e" )]
    public class MASASPublishingWFFeatureEventReceiver : SPFeatureReceiver
    {

        public override void FeatureActivated( SPFeatureReceiverProperties properties )
        {
            //SPWebService contentService = SPWebService.ContentService;
            //contentService.WebConfigModifications.Add( GetConfigModification() );
            //contentService.Update();
            //contentService.ApplyWebConfigModifications();
        }

        public override void FeatureDeactivating( SPFeatureReceiverProperties properties )
        {
            //SPWebService contentService = SPWebService.ContentService;
            //contentService.WebConfigModifications.Remove( GetConfigModification() );
            //contentService.Update();
            //contentService.ApplyWebConfigModifications();
        }

        private SPWebConfigModification GetConfigModification()
        {
            string assemblyValue = typeof( PublishingAction ).Assembly.FullName;
            string namespaceValue = typeof( PublishingAction ).Namespace;

            SPWebConfigModification modification = new SPWebConfigModification(
                string.Format( CultureInfo.CurrentCulture,
                "authorizedType[@Assembly='{0}'][@Namespace='{1}'][@TypeName='*'][@Authorized='True']", assemblyValue, namespaceValue ),
                "configuration/System.Workflow.ComponentModel.WorkflowCompiler/authorizedTypes/targetFx[@version='v4.0']" );

            modification.Owner = "PublishingWorkflowAction";
            modification.Sequence = 0;
            modification.Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode;
            modification.Value =
                string.Format( CultureInfo.CurrentCulture,
                "<authorizedType Assembly=\"{0}\" Namespace=\"{1}\" TypeName=\"*\" Authorized=\"True\" />", assemblyValue, namespaceValue );

            Trace.TraceInformation( "SPWebConfigModification value: {0}", modification.Value );

            return modification;
        }

        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}

    }

}
