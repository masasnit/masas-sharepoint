﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34011
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PublishingWorkflowAction.PublicationService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Hub", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data")]
    [System.SerializableAttribute()]
    public partial class Hub : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TokenField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string URIField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Token {
            get {
                return this.TokenField;
            }
            set {
                if ((object.ReferenceEquals(this.TokenField, value) != true)) {
                    this.TokenField = value;
                    this.RaisePropertyChanged("Token");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string URI {
            get {
                return this.URIField;
            }
            set {
                if ((object.ReferenceEquals(this.URIField, value) != true)) {
                    this.URIField = value;
                    this.RaisePropertyChanged("URI");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PublicationEntry", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PublishingWorkflowAction.PublicationService.FeedEntry))]
    public partial class PublicationEntry : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.Attachment[] AttachmentsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.CategoryTypes[] CategoriesField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.CertaintyTypes[] CertaintiesField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ContentField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime ExpirationField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.Geometry[] GeometriesField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IconField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string[] RelatedLinksField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.SeverityTypes[] SeveritiesField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.StatusTypes StatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SummaryField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TitleField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.Attachment[] Attachments {
            get {
                return this.AttachmentsField;
            }
            set {
                if ((object.ReferenceEquals(this.AttachmentsField, value) != true)) {
                    this.AttachmentsField = value;
                    this.RaisePropertyChanged("Attachments");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.CategoryTypes[] Categories {
            get {
                return this.CategoriesField;
            }
            set {
                if ((object.ReferenceEquals(this.CategoriesField, value) != true)) {
                    this.CategoriesField = value;
                    this.RaisePropertyChanged("Categories");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.CertaintyTypes[] Certainties {
            get {
                return this.CertaintiesField;
            }
            set {
                if ((object.ReferenceEquals(this.CertaintiesField, value) != true)) {
                    this.CertaintiesField = value;
                    this.RaisePropertyChanged("Certainties");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Content {
            get {
                return this.ContentField;
            }
            set {
                if ((object.ReferenceEquals(this.ContentField, value) != true)) {
                    this.ContentField = value;
                    this.RaisePropertyChanged("Content");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime Expiration {
            get {
                return this.ExpirationField;
            }
            set {
                if ((this.ExpirationField.Equals(value) != true)) {
                    this.ExpirationField = value;
                    this.RaisePropertyChanged("Expiration");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.Geometry[] Geometries {
            get {
                return this.GeometriesField;
            }
            set {
                if ((object.ReferenceEquals(this.GeometriesField, value) != true)) {
                    this.GeometriesField = value;
                    this.RaisePropertyChanged("Geometries");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Icon {
            get {
                return this.IconField;
            }
            set {
                if ((object.ReferenceEquals(this.IconField, value) != true)) {
                    this.IconField = value;
                    this.RaisePropertyChanged("Icon");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string[] RelatedLinks {
            get {
                return this.RelatedLinksField;
            }
            set {
                if ((object.ReferenceEquals(this.RelatedLinksField, value) != true)) {
                    this.RelatedLinksField = value;
                    this.RaisePropertyChanged("RelatedLinks");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.SeverityTypes[] Severities {
            get {
                return this.SeveritiesField;
            }
            set {
                if ((object.ReferenceEquals(this.SeveritiesField, value) != true)) {
                    this.SeveritiesField = value;
                    this.RaisePropertyChanged("Severities");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.StatusTypes Status {
            get {
                return this.StatusField;
            }
            set {
                if ((this.StatusField.Equals(value) != true)) {
                    this.StatusField = value;
                    this.RaisePropertyChanged("Status");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Summary {
            get {
                return this.SummaryField;
            }
            set {
                if ((object.ReferenceEquals(this.SummaryField, value) != true)) {
                    this.SummaryField = value;
                    this.RaisePropertyChanged("Summary");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Title {
            get {
                return this.TitleField;
            }
            set {
                if ((object.ReferenceEquals(this.TitleField, value) != true)) {
                    this.TitleField = value;
                    this.RaisePropertyChanged("Title");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="FeedEntry", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data")]
    [System.SerializableAttribute()]
    public partial class FeedEntry : PublishingWorkflowAction.PublicationService.PublicationEntry {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.EntryAuthor AuthorField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IdentifierField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime LastUpdatedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime PublishedField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.EntryAuthor Author {
            get {
                return this.AuthorField;
            }
            set {
                if ((object.ReferenceEquals(this.AuthorField, value) != true)) {
                    this.AuthorField = value;
                    this.RaisePropertyChanged("Author");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Identifier {
            get {
                return this.IdentifierField;
            }
            set {
                if ((object.ReferenceEquals(this.IdentifierField, value) != true)) {
                    this.IdentifierField = value;
                    this.RaisePropertyChanged("Identifier");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime LastUpdated {
            get {
                return this.LastUpdatedField;
            }
            set {
                if ((this.LastUpdatedField.Equals(value) != true)) {
                    this.LastUpdatedField = value;
                    this.RaisePropertyChanged("LastUpdated");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime Published {
            get {
                return this.PublishedField;
            }
            set {
                if ((this.PublishedField.Equals(value) != true)) {
                    this.PublishedField = value;
                    this.RaisePropertyChanged("Published");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Attachment", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data")]
    [System.SerializableAttribute()]
    public partial class Attachment : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string Base64Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ContentTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FileNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TitleField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Base64 {
            get {
                return this.Base64Field;
            }
            set {
                if ((object.ReferenceEquals(this.Base64Field, value) != true)) {
                    this.Base64Field = value;
                    this.RaisePropertyChanged("Base64");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ContentType {
            get {
                return this.ContentTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.ContentTypeField, value) != true)) {
                    this.ContentTypeField = value;
                    this.RaisePropertyChanged("ContentType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FileName {
            get {
                return this.FileNameField;
            }
            set {
                if ((object.ReferenceEquals(this.FileNameField, value) != true)) {
                    this.FileNameField = value;
                    this.RaisePropertyChanged("FileName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Title {
            get {
                return this.TitleField;
            }
            set {
                if ((object.ReferenceEquals(this.TitleField, value) != true)) {
                    this.TitleField = value;
                    this.RaisePropertyChanged("Title");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CategoryTypes", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.DomainLayer.Model")]
    public enum CategoryTypes : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Other = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Geo = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Met = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Safety = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Security = 4,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Rescue = 5,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Fire = 6,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Health = 7,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Env = 8,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Transport = 9,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Infra = 10,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        CBRNE = 11,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CertaintyTypes", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.DomainLayer.Model")]
    public enum CertaintyTypes : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Unknown = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Observed = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Likely = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Possible = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Unlikely = 4,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Geometry", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data.Geometries")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PublishingWorkflowAction.PublicationService.GeoCircle))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PublishingWorkflowAction.PublicationService.GeoPoint))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PublishingWorkflowAction.PublicationService.GeoLine))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PublishingWorkflowAction.PublicationService.GeoPolygon))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(PublishingWorkflowAction.PublicationService.GeoBox))]
    public partial class Geometry : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SeverityTypes", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.DomainLayer.Model")]
    public enum SeverityTypes : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Unknown = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Extreme = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Severe = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Moderate = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Minor = 4,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="StatusTypes", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.DomainLayer.Model")]
    public enum StatusTypes : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Test = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Actual = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Exercise = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        System = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Draft = 4,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GeoCircle", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data.Geometries")]
    [System.SerializableAttribute()]
    public partial class GeoCircle : PublishingWorkflowAction.PublicationService.Geometry {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.GeoPoint CenterPointField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double RadiusField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.GeoPoint CenterPoint {
            get {
                return this.CenterPointField;
            }
            set {
                if ((object.ReferenceEquals(this.CenterPointField, value) != true)) {
                    this.CenterPointField = value;
                    this.RaisePropertyChanged("CenterPoint");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Radius {
            get {
                return this.RadiusField;
            }
            set {
                if ((this.RadiusField.Equals(value) != true)) {
                    this.RadiusField = value;
                    this.RaisePropertyChanged("Radius");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GeoPoint", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data.Geometries")]
    [System.SerializableAttribute()]
    public partial class GeoPoint : PublishingWorkflowAction.PublicationService.Geometry {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double LatitudeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double LongitudeField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Latitude {
            get {
                return this.LatitudeField;
            }
            set {
                if ((this.LatitudeField.Equals(value) != true)) {
                    this.LatitudeField = value;
                    this.RaisePropertyChanged("Latitude");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Longitude {
            get {
                return this.LongitudeField;
            }
            set {
                if ((this.LongitudeField.Equals(value) != true)) {
                    this.LongitudeField = value;
                    this.RaisePropertyChanged("Longitude");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GeoLine", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data.Geometries")]
    [System.SerializableAttribute()]
    public partial class GeoLine : PublishingWorkflowAction.PublicationService.Geometry {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.GeoPoint[] PointsField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.GeoPoint[] Points {
            get {
                return this.PointsField;
            }
            set {
                if ((object.ReferenceEquals(this.PointsField, value) != true)) {
                    this.PointsField = value;
                    this.RaisePropertyChanged("Points");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GeoPolygon", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data.Geometries")]
    [System.SerializableAttribute()]
    public partial class GeoPolygon : PublishingWorkflowAction.PublicationService.Geometry {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.GeoPoint[] PointsField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.GeoPoint[] Points {
            get {
                return this.PointsField;
            }
            set {
                if ((object.ReferenceEquals(this.PointsField, value) != true)) {
                    this.PointsField = value;
                    this.RaisePropertyChanged("Points");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GeoBox", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.Services.Data.Geometries")]
    [System.SerializableAttribute()]
    public partial class GeoBox : PublishingWorkflowAction.PublicationService.Geometry {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.GeoPoint LowerCornerField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private PublishingWorkflowAction.PublicationService.GeoPoint UpperCornerField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.GeoPoint LowerCorner {
            get {
                return this.LowerCornerField;
            }
            set {
                if ((object.ReferenceEquals(this.LowerCornerField, value) != true)) {
                    this.LowerCornerField = value;
                    this.RaisePropertyChanged("LowerCorner");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public PublishingWorkflowAction.PublicationService.GeoPoint UpperCorner {
            get {
                return this.UpperCornerField;
            }
            set {
                if ((object.ReferenceEquals(this.UpperCornerField, value) != true)) {
                    this.UpperCornerField = value;
                    this.RaisePropertyChanged("UpperCorner");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="EntryAuthor", Namespace="http://schemas.datacontract.org/2004/07/MASAS.MSM.DomainLayer.Model")]
    [System.SerializableAttribute()]
    public partial class EntryAuthor : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string EmailField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string UriField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email {
            get {
                return this.EmailField;
            }
            set {
                if ((object.ReferenceEquals(this.EmailField, value) != true)) {
                    this.EmailField = value;
                    this.RaisePropertyChanged("Email");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Uri {
            get {
                return this.UriField;
            }
            set {
                if ((object.ReferenceEquals(this.UriField, value) != true)) {
                    this.UriField = value;
                    this.RaisePropertyChanged("Uri");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="PublicationService.IPublicationService")]
    public interface IPublicationService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPublicationService/PublishEntry", ReplyAction="http://tempuri.org/IPublicationService/PublishEntryResponse")]
        PublishingWorkflowAction.PublicationService.FeedEntry PublishEntry(PublishingWorkflowAction.PublicationService.Hub hub, PublishingWorkflowAction.PublicationService.PublicationEntry publicationEntry);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPublicationService/UpdateEntry", ReplyAction="http://tempuri.org/IPublicationService/UpdateEntryResponse")]
        PublishingWorkflowAction.PublicationService.FeedEntry UpdateEntry(PublishingWorkflowAction.PublicationService.Hub hub, string entryIdentifier, PublishingWorkflowAction.PublicationService.PublicationEntry publicationEntry);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IPublicationServiceChannel : PublishingWorkflowAction.PublicationService.IPublicationService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PublicationServiceClient : System.ServiceModel.ClientBase<PublishingWorkflowAction.PublicationService.IPublicationService>, PublishingWorkflowAction.PublicationService.IPublicationService {
        
        public PublicationServiceClient() {
        }
        
        public PublicationServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PublicationServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PublicationServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PublicationServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public PublishingWorkflowAction.PublicationService.FeedEntry PublishEntry(PublishingWorkflowAction.PublicationService.Hub hub, PublishingWorkflowAction.PublicationService.PublicationEntry publicationEntry) {
            return base.Channel.PublishEntry(hub, publicationEntry);
        }
        
        public PublishingWorkflowAction.PublicationService.FeedEntry UpdateEntry(PublishingWorkflowAction.PublicationService.Hub hub, string entryIdentifier, PublishingWorkflowAction.PublicationService.PublicationEntry publicationEntry) {
            return base.Channel.UpdateEntry(hub, entryIdentifier, publicationEntry);
        }
    }
}
