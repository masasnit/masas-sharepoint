﻿using System;
using System.ComponentModel;

using Microsoft.SharePoint.WebPartPages;

namespace MASAS.Feature.Addons.WebParts.NotificationDashboard
{

    /// <summary>
    /// Class NotificationDashboard.
    /// </summary>
    [ToolboxItemAttribute( false )]
    public class NotificationDashboard : WebPart
    {

        // Private members...

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/15/MASAS/Addons/WebParts/NotificationDashboard/NotificationDashboardUserControl.ascx";

        private NotificationDashboardUserControl _control = null;

        private bool    _showIgnored            = false;
        private string  _listName               = "MASAS Hub Items";
        private string  _expirationFieldName    = "MASAS_Expiration";
        private string  _trackingFieldName      = "MASAS_TrackingState";

        /// <summary>
        /// Gets or sets a value indicating whether [show ignored].
        /// </summary>
        /// <value><c>true</c> if [show ignored]; otherwise, <c>false</c>.</value>
        public bool ShowIgnored
        {
            get { return _showIgnored; }
            set { _showIgnored = value; }
        }

        /// <summary>
        /// Gets or sets the name of the list.
        /// </summary>
        /// <value>The name of the list.</value>
        public string ListName
        {
            get { return _listName; }
            set { _listName = value; }
        }

        /// <summary>
        /// Gets or sets the name of the expiration field.
        /// </summary>
        /// <value>The name of the expiration field.</value>
        public string ExpirationFieldName
        {
            get { return _expirationFieldName; }
            set { _expirationFieldName = value; }
        }

        /// <summary>
        /// Gets or sets the name of the tracking field.
        /// </summary>
        /// <value>The name of the tracking field.</value>
        public string TrackingFieldName
        {
            get { return _trackingFieldName; }
            set { _trackingFieldName = value; }
        }

        /// <summary>
        /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
        /// </summary>
        protected override void CreateChildControls()
        {
            _control = Page.LoadControl( _ascxPath ) as NotificationDashboardUserControl;

            _control.Attributes.Add( "ShowIgnored", ShowIgnored.ToString() );
            _control.Attributes.Add( "ListName", ListName );
            _control.Attributes.Add( "ExpirationFieldName", ExpirationFieldName );
            _control.Attributes.Add( "TrackingFieldName", TrackingFieldName );

            Controls.Add( _control );
        }

        /// <summary>
        /// Updates the attributes.
        /// </summary>
        public void UpdateAttributes()
        {
            _control.Attributes["ShowIgnored"] = ShowIgnored.ToString();
            _control.Attributes["ListName"] = ListName;
            _control.Attributes["ExpirationFieldName"] = ExpirationFieldName;
            _control.Attributes["TrackingFieldName"] = TrackingFieldName;
            _control.UpdateAttributes();
        }

        /// <summary>
        /// Determines which tool parts are displayed in the tool pane of the Web-based Web Part design user interface, and the order in which they are displayed.
        /// </summary>
        /// <returns>An array of type <see cref="T:Microsoft.SharePoint.WebPartPages.ToolPart" /> that determines which tool parts will be displayed in the tool pane. If a Web Part that implements one or more custom properties does not override the <see cref="M:Microsoft.SharePoint.WebPartPages.WebPart.GetToolParts" /> method, the base class method will return an instance of the <see cref="T:Microsoft.SharePoint.WebPartPages.WebPartToolPart" /> class and an instance of the <see cref="T:Microsoft.SharePoint.WebPartPages.CustomPropertyToolPart" /> class. An instance of the <see cref="T:Microsoft.SharePoint.WebPartPages.WebPartToolPart" /> class displays a tool part for working with the properties provided by the WebPart base class. An instance of the <see cref="T:Microsoft.SharePoint.WebPartPages.CustomPropertyToolPart" /> class displays a built-in tool part for working custom Web Part properties, as long as the custom property is of one of the types supported by that tool part. The supported types are: String, Boolean, Integer, DateTime, or Enum.</returns>
        public override ToolPart[] GetToolParts()
        {
            ToolPart[] allToolParts = new ToolPart[3];
            WebPartToolPart standardToolParts = new WebPartToolPart();
            CustomPropertyToolPart customToolParts = new CustomPropertyToolPart();
            allToolParts[0] = standardToolParts;
            allToolParts[1] = customToolParts;
            allToolParts[2] = new NotificationDashboardToolPart();
            return allToolParts;
        }

    }

}
