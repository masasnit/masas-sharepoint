﻿using System;

namespace MASAS.Feature.Addons.WebParts.NotificationDashboard
{

    /// <summary>
    /// Dashboard Data Class.
    /// Container to bind data to the control.
    /// </summary>
    public class DashboardData
    {

        /// <summary>
        /// Gets or sets the number of action items.
        /// </summary>
        /// <value>The number of action items.</value>
        public int NumberOfActionItems { get; set; }

        /// <summary>
        /// Gets or sets the number of ignored items.
        /// </summary>
        /// <value>The number of ignored items.</value>
        public int NumberOfIgnoredItems { get; set; }

        /// <summary>
        /// Gets or sets the number of monitor items.
        /// </summary>
        /// <value>The number of monitor items.</value>
        public int NumberOfMonitorItems { get; set; }

        /// <summary>
        /// Gets or sets the number of none items.
        /// </summary>
        /// <value>The number of none items.</value>
        public int NumberOfNoneItems { get; set; }

        /// <summary>
        /// Gets or sets the tolal number of items.
        /// </summary>
        /// <value>The tolal number of items.</value>
        public int TotalNumberOfItems { get; set; }

        /// <summary>
        /// Clears the totals.
        /// </summary>
        internal void ClearTotals()
        {
            NumberOfActionItems = 0;
            NumberOfIgnoredItems = 0;
            NumberOfMonitorItems = 0;
            NumberOfNoneItems = 0;
            TotalNumberOfItems = 0;
        }

    }

}
