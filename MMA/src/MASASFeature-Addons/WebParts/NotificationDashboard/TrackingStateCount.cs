﻿using System;

namespace MASAS.Feature.Addons.WebParts.NotificationDashboard
{

    /// <summary>
    /// Class TrackingStateCount.
    /// </summary>
    public class TrackingStateCount
    {

        /// <summary>
        /// Gets or sets the state of the tracking.
        /// </summary>
        /// <value>The state of the tracking.</value>
        public string TrackingState { get; set; }

        /// <summary>
        /// Gets or sets the tracking count.
        /// </summary>
        /// <value>The tracking count.</value>
        public int TrackingCount { get; set; }

    }

}
