﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationDashboardUserControl.ascx.cs" Inherits="MASAS.Feature.Addons.WebParts.NotificationDashboard.NotificationDashboardUserControl" %>

<!-- Custom CSS for the web part -->
<link href="/_layouts/15/MASAS/Addons/WebParts/NotificationDashboard/layout.css" rel="stylesheet" type="text/css" />

<!-- Notification Dashboard controls -->
<asp:UpdatePanel ID="mainUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    
        <asp:Timer ID="refreshTimer" interval="30000" runat="server" OnTick="refreshTimer_Tick" />

        <div class="masas-ndwp-mainContainer">

            <div id="grpAction" class="masas-ndwp-trackingNV" runat="server">
                <div id="lblAction" class="masas-ndwp-label" runat="server">Action:</div>
                <div id="lblActionValue" class="masas-ndwp-value" runat="server">0</div>
            </div>

            <div id="grpMonitor" class="masas-ndwp-trackingNV" runat="server">
                <div id="lblMonitor" class="masas-ndwp-label" runat="server">Monitor:</div>
                <div id="lblMonitorValue" class="masas-ndwp-value" runat="server">0</div>
            </div>

            <div id="grpNone" class="masas-ndwp-trackingNV" runat="server">
                <div id="lblNone" class="masas-ndwp-label" runat="server">None:</div>
                <div id="lblNoneValue" class="masas-ndwp-value" runat="server">0</div>
            </div>

            <div id="grpIgnore" class="masas-ndwp-trackingNV" runat="server">
                <div id="lblIgnore" class="masas-ndwp-label" runat="server">Ignore:</div>
                <div id="lblIgnoreValue" class="masas-ndwp-value" runat="server">0</div>
            </div>

            <div id="grpTotal" class="masas-ndwp-trackingNV" runat="server">
                <div id="lblTotal" class="masas-ndwp-label" runat="server">Total:</div>
                <div id="lblTotalValue" class="masas-ndwp-value" runat="server">0</div>
            </div>

        </div>

    </ContentTemplate>
</asp:UpdatePanel>
