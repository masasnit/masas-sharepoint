﻿using System;

namespace MASAS.Feature.Addons.WebParts.NotificationDashboard
{

    /// <summary>
    /// Class HubItem.
    /// </summary>
    public class HubItem
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Identifier { get; set; }

        /// <summary>
        /// Gets or sets the state of the tracking.
        /// </summary>
        /// <value>The state of the tracking.</value>
        public string TrackingState { get; set; }

    }

}
