﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace MASASFeature.WebParts.NotificationPanelWebPart
{

    [ToolboxItemAttribute( false )]
    public class NotificationPanelWebPart : WebPart
    {

        protected override void CreateChildControls()
        {
        }

    }

}
