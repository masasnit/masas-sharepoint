﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Xsl;
using System.Text;
using System.IO;

using Microsoft.SharePoint;

namespace MASASFeature.WebParts.ViewAlertWebPart
{

    /// <summary>
    /// View Alert Web Part
    /// Display a Hub Item's Alert XML content formated with XSL.
    /// </summary>
    public partial class ViewAlertWebPartUserControl : UserControl
    {

        // Private members...
        private string _listId      = string.Empty;
        private string _hubItemId   = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load( object sender, EventArgs e )
        {
            // List Id...
            if( Request.QueryString["ListId"] != null )
            {
                _listId = Request.QueryString["ListId"];
            }

            // Item Id...
            if( Request.QueryString["HubItemID"] != null )
            {
                _hubItemId = Request.QueryString["HubItemID"];
            }
        }

        /// <summary>
        /// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the server control content.</param>
        protected override void Render( HtmlTextWriter writer )
        {
            base.Render( writer );

            if( _listId != string.Empty && _hubItemId != string.Empty )
            {
                LoadHubItemAlert( writer );
            }
        }

        /// <summary>
        /// Loads the Hub Item's Alert content into the control.
        /// </summary>
        /// <param name="htmlWriter">The HTML writer.</param>
        private void LoadHubItemAlert( HtmlTextWriter htmlWriter )
        {
            SPWeb web = SPContext.Current.Web;
            SPList list = null;

            try
            {
                // Get the List...
                list = web.Lists.GetList( new Guid( _listId ), false );
            }
            catch( Exception /*e*/ )
            {
            }

            if( list != null )
            {
                SPListItem listItem = null;

                try
                {
                    // Get the Item...
                    listItem = list.GetItemById( Convert.ToInt32( _hubItemId ) );
                }
                catch( Exception /*e*/ )
                {
                }

                // We need a specific field...
                if( listItem != null && listItem["MASAS_EnclosureXML"] != null )
                {
                    // Get the contents of the field for the XML control...
                    string xml = listItem["MASAS_EnclosureXML"].ToString();

                    // What version of CAP are we working with?
                    string strCapNs_v12 = "urn:oasis:names:tc:emergency:cap:1.2";
                    string xsltFile = "";

                    if( xml.IndexOf( strCapNs_v12 ) > 0 )
                    {
                        xsltFile = "MASASDocuments/CAP_v12.xslt";
                    }
                    else
                    {
                        xsltFile = "MASASDocuments/CAP.xslt";
                    }

                    // Get the xslt for the XML control..
                    SPFile fileToRead = web.GetFile( xsltFile );

                    if( fileToRead.Exists == true )
                    {
                        string xsl = web.GetFileAsString( xsltFile );

                        using( StringReader xslStream = new StringReader( xsl ) )
                        using( StringReader xmlStream = new StringReader( xml ) )
                        {
                            using( XmlReader xslReader = XmlReader.Create( xslStream ) )
                            using( XmlReader xmlReader = XmlReader.Create( xmlStream ) )
                            {
                                XslCompiledTransform xslt = new XslCompiledTransform();
                                xslt.Load( xslReader );
                                xslt.Transform( xmlReader, null, htmlWriter );
                            }
                        }
                    }
                    else
                    {
                        htmlWriter.Write( "<p>The required XSLT file could not be found (" + xsltFile + ")</p>" );
                    }
                }
            }
        }

    }

}
