using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Security;
using Microsoft.SharePoint.Workflow;

namespace MASASFeature.Features.MASAS
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>
    [Guid( "f14e7522-e40d-46d2-aa20-67d146a4aa1a" )]
    public class MASASEventReceiver : SPFeatureReceiver
    {

        /// <summary>
        /// Occurs after a Feature is activated.
        /// </summary>
        /// <param name="properties">An <see cref="T:Microsoft.SharePoint.SPFeatureReceiverProperties"/> object that represents the properties of the event.</param>
        public override void FeatureActivated( SPFeatureReceiverProperties properties )
        {
            //SPWeb web = properties.Feature.Parent as SPWeb;

            //// Verify we have our Tasks and Workflow History lists...
            //CreateLists( web );

            //// Get the Publications list...
            //SPList list = web.Lists.TryGetList( "MASAS Publications" );

            //if( list != null )
            //{
            //    // Get the SPContentTypeId object for the content type...
            //    SPContentTypeId parentContentTypeId = new SPContentTypeId( "0x01" );

            //    // The Guid passed here should match the BaseID attribute of the <Template> element within the workflow's .wfconfig.xml file...
            //    AssociateWorkflow( list, new Guid( "342358A4-E84B-4E76-9870-C51CFD405B72" ), "MASAS Publication Workflow", parentContentTypeId );
            //}
        }
        
        /// <summary>
        /// Creates the lists as needed.
        /// </summary>
        /// <param name="web">The web.</param>
        //private void CreateLists( SPWeb web )
        //{
        //    SPList historyList = web.Lists.TryGetList( "Workflow History" );
        //    SPList taskList = web.Lists.TryGetList( "Tasks" );

        //    // If no Task list exists, create one...
        //    if( taskList == null )
        //    {
        //        Guid listGuid = web.Lists.Add( "Tasks", "Task list for workflow", SPListTemplateType.Tasks );
        //        taskList = web.Lists[listGuid];
        //        taskList.Update();
        //    }

        //    // If no Workflow History list exists, create one...
        //    if( historyList == null )
        //    {
        //        Guid listGuid = web.Lists.Add( "Workflow History", "History list for workflow", SPListTemplateType.WorkflowHistory );
        //        taskList = web.Lists[listGuid];
        //        taskList.Hidden = true;
        //        taskList.Update();
        //    }
        //}

        /// <summary>
        /// Associates a workflow to a list.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="wfId">The wf id.</param>
        /// <param name="wfName">Name of the wf.</param>
        /// <param name="baseContentTypeId">The base content type id.</param>
        //private void AssociateWorkflow( SPList list, Guid wfId, string wfName, SPContentTypeId baseContentTypeId )
        //{
        //    SPList historyList = list.ParentWeb.Lists.TryGetList( "Workflow History" );
        //    SPList taskList = list.ParentWeb.Lists.TryGetList( "Tasks" );

        //    if( taskList != null && historyList != null )
        //    {
        //        SPWorkflowTemplate wfTemplate = list.ParentWeb.WorkflowTemplates.GetTemplateByBaseID( wfId );

        //        if( wfTemplate != null )
        //        {
        //            SPWorkflowAssociation wfAssociation = SPWorkflowAssociation.CreateListContentTypeAssociation( wfTemplate, wfName, taskList, historyList );

        //            wfAssociation.AllowManual = true;

        //            bool allowUnsafe = list.ParentWeb.AllowUnsafeUpdates;
        //            list.ParentWeb.AllowUnsafeUpdates = true;
        //            try
        //            {
        //                foreach( SPContentType ct in list.ContentTypes )
        //                {
        //                    if( ct.Id.IsChildOf( baseContentTypeId ) )
        //                    {
        //                        if( ct.WorkflowAssociations.GetAssociationByName( wfName, System.Globalization.CultureInfo.CurrentUICulture ) == null )
        //                        {
        //                            ct.WorkflowAssociations.Add( wfAssociation );
        //                        }
        //                    }
        //                }
        //            }
        //            catch( Exception /* ex */ )
        //            {
        //                // Logging...
        //            }
        //            finally
        //            {
        //                list.ParentWeb.AllowUnsafeUpdates = allowUnsafe;
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Occurs when a Feature is deactivated.
        /// </summary>
        /// <param name="properties">An <see cref="T:Microsoft.SharePoint.SPFeatureReceiverProperties"/> object that represents the properties of the event.</param>
        public override void FeatureDeactivating( SPFeatureReceiverProperties properties )
        {
            // NOTE: This functionality is disabled for now.
            //SPWeb web = properties.Feature.Parent as SPWeb;

            //// We need to remove the lists we created...
            //DeleteList( web.Lists, "MASAS Hub Items" );
            //DeleteList( web.Lists, "MASAS Hub Filters" );
            //DeleteList( web.Lists, "MASAS Publications" );
            //DeleteList( web.Lists, "MASAS Notifications" );
            //DeleteList( web.Lists, "MASAS Hub Connections" );
            //DeleteList( web.Lists, "MASAS Documents" );

            //// Remove the "MASAS" folder...
            //SPFolder folder = web.GetFolder( "MASAS" );
            //if( folder != null )
            //{
            //    folder.Delete();
            //}
        }

        /// <summary>
        /// Deletes the list.
        /// </summary>
        private void DeleteList( SPListCollection lists, string listName )
        {
            SPList list = lists.TryGetList( listName );

            if( list != null )
            {
                lists.Delete( list.ID );
            }
        }


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
