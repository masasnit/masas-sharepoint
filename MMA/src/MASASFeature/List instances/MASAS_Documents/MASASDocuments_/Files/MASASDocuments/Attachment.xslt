<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:georss="http://www.georss.org/georss"
	xmlns:app="http://www.w3.org/2007/app"
	xmlns:age="http://purl.org/atompub/age/1.0"
	xmlns:met="masas:experimental:time"
	xmlns:clitype="clitype"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:link="http://www.xbrl.org/2003/linkbase"
	xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="atom georss app age met clitype fn link xlink xs xsi">
  <xsl:output version="4.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>

  <xsl:variable name="XML" select="/"/>

  <xsl:template match="/">
    <html>
      <head>
        <title/>
        <style type="text/css">
          .TemplateDisplayBox { margin: 5px; padding: 5px; border: 1px solid black; }
        </style>
        <script language="javascript">
          function onLinkClicked( link )
          {
            var accessCode = document.getElementsByName('accessCode')[0].value;
            var newLink = link + '?secret=' + accessCode;
            window.location.href = newLink;
          }
        </script>

      </head>
      <body>
        
        <xsl:for-each select="atom:entry">
          <h1>
            <xsl:text>Attachments</xsl:text>
          </h1>

          <div>
            <xsl:text>Access Code </xsl:text>
            <input type="password" name="accessCode" />
          </div>
          <br/>
          
          <div class="TemplateDisplayBox">
            <table border="0">
              <tbody>
                <tr>
                  <td>
                    <b>
                      <xsl:text>Title</xsl:text>
                    </b>
                  </td>
                  <td>
                    <b>
                      <xsl:text>Link</xsl:text>
                    </b> 
                  </td>
                  <td>
                    <b>
                      <xsl:text>Type</xsl:text>
                    </b>
                  </td>
                  <td>
                    <b>
                      <xsl:text>Length</xsl:text>
                    </b>
                  </td>
                </tr>

                <xsl:for-each select="atom:link[@rel='enclosure']">
                  <tr>
                      <xsl:call-template name="Link"/>
                  </tr>
                </xsl:for-each>
                
              </tbody>
            </table>
          </div>
          
        </xsl:for-each>
        <br/>

      </body>
    </html>
  </xsl:template>

  <xsl:template name="Link">
    <td>
      <xsl:value-of select="@title"/>
    </td>
    <td>      
      <a href="#">
        <xsl:attribute name="onclick">
          javascript:onLinkClicked( '<xsl:value-of select="@href"/>' );
        </xsl:attribute>
        <xsl:value-of select="@href"/>
      </a>
    </td>
    <td>
      <xsl:value-of select="@type"/>
    </td>
    <td>
      <xsl:value-of select="@length"/>
    </td>
  </xsl:template>

</xsl:stylesheet>
